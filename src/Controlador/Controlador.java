package Controlador;
import Modelo.Bomba;
import Modelo.Gasolina;
import Vista.Vista;
import javax.swing.JFrame;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JOptionPane;

public class Controlador implements ActionListener {

    private Bomba bomba;
    private Gasolina ga;
    private Vista vista;
    
    
    
    
    public Controlador(Vista v, Bomba b){
        
        this.vista = v;
        this.bomba = b;
        
        vista.btnIniciarBomba.addActionListener(this);
        vista.btnRegistrar.addActionListener(this);
        vista.cboTipoGas.addActionListener(this);
        vista.btnSalir.addActionListener(this);
        
        
        
    }
    
    private void iniciarVista(){
                
        vista.setTitle(": :     GASOLINERA       : :");
        vista.setSize(800,560);
        vista.setVisible(true);
    }
    


    @Override
    public void actionPerformed(ActionEvent e) {
        
        if(e.getSource()==vista.btnIniciarBomba){
            
            try{
                //Capturar datos de txtflds en atributos 
                int num = Integer.parseInt(vista.txtNumBomba.getText());
                float pre = Float.parseFloat(vista.txtPrecioVenta.getText());
                String tp = vista.cboTipoGas.getSelectedItem().toString();

                //Pasar parametros a clase e inicializar datos 
                bomba.iniciarBomba(pre,num , tp );

                vista.txtContadorVentas.setText("0");
            } catch(NumberFormatException ex){
                JOptionPane.showMessageDialog(vista, "Surgió el siguiente error: " + ex.getMessage());
                return;
            }
             catch(Exception ex2){
                
                JOptionPane.showMessageDialog(vista, "Surgió el siguiente error: " + ex2.getMessage());
                return;
            }
            
            //activar botones y txtfields
            vista.txtCantitad.setEnabled(true);
            vista.txtCosto.setEnabled(true);
            vista.txtTotalVentas.setEnabled(true);
            vista.btnRegistrar.setEnabled(true);
            
            //Desactivar txtfields, boton y combobox
            
            vista.txtPrecioVenta.setEnabled(false);
            vista.txtContadorVentas.setEnabled(false);
            vista.cboTipoGas.setEnabled(false);
            vista.txtNumBomba.setEnabled(false);            
            vista.btnIniciarBomba.setEnabled(false);
            
            vista.sldCapacidad.setValue(200);
            
            }
            
            if(e.getSource()==vista.btnRegistrar){
                
                // Se crea la variable que guarde el importe dentro del bloque btnRegistar
                float importe;
                
                try{
                    // Se intenta hacer la venta y se regresa un valor
                    importe = bomba.venderGasolina(Float.parseFloat(vista.txtCantitad.getText()));
                    
                    // La cantidad de gasolina era mayor a la cantidad restante
                    if(importe <= 0){
                        JOptionPane.showMessageDialog(vista, "No hay suficiente gasolina " );
                        return;
                    }
                        
                    // La cantidad de gasolina a vender es menor o igual que la que hay para  
                    else {
                        
                        // SE 
                        vista.txtCosto.setText(Float.toString(importe));
                        vista.txtTotalVentas.setText(Float.toString(bomba.ventasTotales()));
                        vista.txtContadorVentas.setText(Integer.toString(Integer.parseInt(vista.txtContadorVentas.getText())+1));
                        vista.sldCapacidad.setValue((int)bomba.inventarioGasolina());
                        JOptionPane.showMessageDialog(vista, "GRACIAS VUELVA PRONTO " );
                        
                    }
                }
                catch(NumberFormatException ex){
                    JOptionPane.showMessageDialog(vista, "Error: " + ex.getMessage());
                }
                
            }
            
            if(e.getSource()==vista.btnSalir){
                int option = JOptionPane.showConfirmDialog(vista, "¿Desea salir?",
                        "Seleccione", JOptionPane.YES_NO_OPTION);
                if(option == JOptionPane.YES_NO_OPTION){
                    vista.dispose();
                    System.exit(0);
            }
        }
        
    }
    
        public static void main(String[] args) {
        
        // EL CONSTRUCTOR DE VISTA OCUPA COMO PARAMETROS EL UN OBJETO DE LA CLASE DE JFRAME Y UN MODALE TRUE
        Vista vista = new Vista(new JFrame(), true);
        Bomba bomba = new Bomba();
        Controlador con = new Controlador(vista,bomba);
        con.iniciarVista();
        
    }
    
}
