
package Modelo;


public class Bomba {
    

    private int numBomba;
    private float capacidad, acumulador;
    private Gasolina gas;

    public Bomba(int nb, float cap, float ac, Gasolina obg) {
        this.numBomba = nb;
        this.capacidad = cap;
        this.acumulador = ac;
        this.gas = obg;
    }
    
    //CONSTRUCTOR CLONAR OBJETO
    public Bomba(Bomba ob, Gasolina obg){
        this.numBomba = ob.numBomba;
        this.capacidad = ob.capacidad;
        this.acumulador = ob.acumulador;
        this.gas = obg;
        
    }
    
    //CONSTRUCTOR VACIO
    public Bomba(){
        this.numBomba = 0;
        this.capacidad = 0;
        this.acumulador = 0;
        gas = new Gasolina();
        
    }
    
    //SETS
    public void setNumBomba(int nb){
        numBomba = nb;
    }
    public void setCapacidad(float cap){
        capacidad = cap;
    }
    public void setAcumulador(float ac){
        acumulador = ac;
    }

    public void setGas(Gasolina gas){
        this.gas.setPrecio(gas.getPrecio());
        this.gas.setTipo(gas.getTipo());
    }
    
    
    
    //GETS
    public int getNumBomba(){
        return numBomba;
    }
    public float getCapacidad(){
        return capacidad;
    }
    public float getAcumulador(){
        return acumulador;
    }
    public Gasolina getGas() {
        return gas;
    }
    
    
    
    //INICIAR BOMBA
    public void iniciarBomba(float pr,int nb,String tp){
       this.numBomba = nb;
       this.capacidad = 200f;
       this.acumulador = 0;
       gas.setPrecio(pr);
       gas.setTipo(tp);
        
    }
    
    //Inventario de gasolina
    public float inventarioGasolina(){
        return capacidad - acumulador;
    }
    
    //Venta de gasolina
    public float venderGasolina(float ventaG){
        //No hay suficiente gasolina
        if(ventaG>inventarioGasolina()){
           return -1; 
        }
        //Se realiza la venta
        else{    
            acumulador += ventaG;
            return ventaG * gas.getPrecio();
        }
    }
    
    //Total de ganancias
    public float ventasTotales(){
        return acumulador * gas.getPrecio();
    }
    
}
