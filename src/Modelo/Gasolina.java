
package Modelo;


public class Gasolina {
    
    private String gasID, tipo;
    private float precio;
    
    
    //CONSTRUCTOR CAPTURAR DATOS
    public Gasolina(String gd, String tp, float pc){
        this.gasID=gd;
        this.tipo=tp;
        this.precio=pc;
    }
    
    //CONSTRUCTOR COPIAR OBJETO
    public Gasolina(Gasolina ob){
        this.gasID=ob.gasID;
        this.tipo=ob.tipo;
        this.precio=ob.precio;
    }
    
    //CONSTRUCTOR VACIO
    public Gasolina(){
        this.gasID="";
        this.tipo="";
        this.precio=0;
    }
    
    
    
    //SETS
    public void setGasID(String id){
       gasID=id;
    }
    public void setTipo(String tip){
        tipo=tip;
    }
    public void setPrecio(float pre){
        precio=pre;
    }
    
    
    
    
    //GETS
    public String getGasID(){
        return gasID;
    }
    public String getTipo(){
        return tipo;
    }
    public float getPrecio(){
        return precio;
    }
}
